# Master Cloud Setup Guide

## This repo will help you setup automatically via:
### Terraform:
    - Google Cloud:
        - Kubernetes
        - Managed Gcloud Postgres DB
        - Gcloud Bucket Storage

### Helm:
    - Letsencrypt certificate auto creation/renewal
    - Konghq Api Gateway

### Gitlab CI/CD:
    - Python + Django RestFramework simple API (connected with Gcloud Postgres)
    - Angular Frontend App (showing info from Django backend via API)


# Steps:
## Prep work:
- create Google cloud account + billing + project
- setup your account and setup connection via gcloud
- setup service account with your desired privileges (e.g. for cicd pipeline)
	- save securely cred.json

## Terraform
clone my [terraform repo](https://gitlab.com/daniel-svoboda/devops/terraform)  
copy cred.json into the directory  
follow readme instructions

### Note for DBs:
allow connection to come from public IP of your GKE cluster or IP you will connect to DB from


## Helm
you may follow [helm-setup](https://gitlab.com/daniel-svoboda/devops/helm-setup/tree/master) repo and setup your helm locally to point to the cluster

### Letsencrypt
clone [gke auto letsencrypt](https://gitlab.com/daniel-svoboda/devops/gke-auto-letsencrypt) repo and follow instructions

### Konghq
clone [kong](https://gitlab.com/daniel-svoboda/devops/kong) repo and follow instructions


## Backend Application
clone [Backend App](https://gitlab.com/daniel-svoboda/dev/app-one-backend) repo and follow instructions

## Frontend Application
clone [Frontend App](https://gitlab.com/daniel-svoboda/dev/app-one-frontend) repo and follow instructions


